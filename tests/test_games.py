import falcon
from falcon import testing

import pytest
import json

from nerdar.wsgi_app import application
from nerds import nerds

@pytest.fixture
def client():
    """Sets up mock client to be used for testing"""
    return testing.TestClient(application)

def test_get_games_at_time(client):
    """Tests if portion of lists of game names and times matches expected."""
    response = client.simulate_get('/games/45')
    result_doc = json.loads(response.content.decode('utf-8'))

    assert response.status == falcon.HTTP_OK

def test_get_games_ratings(client):
    """Tests if portion of list of games and ratings matches expected """
    response = client.simulate_get('/games/ratings')
    result_doc = json.loads(response.content.decode('utf-8'))

    assert response.status == falcon.HTTP_OK

def test_post(client):
    pass
