import falcon
from falcon import testing

import pytest
import json

from nerdar.wsgi_app import application
from nerds import nerds

@pytest.fixture
def client():
    """Sets up mock client to be used for testing"""
    return testing.TestClient(application)

def test_get(client):
    """Tests if portion of list of users matches expected."""
    response = client.simulate_get('/nerds')
    result_doc = json.loads(response.content.decode('utf-8'))

    doc = {
        '1' : {'user_name' : 'edgyusername', 'email' : 'user@usee.com', 'password' : 'parameterized'},
        '2' : {'user_name' : 'neato', 'email' : 'neato@usee.com', 'password' : 'passworder'}
    }

    assert result_doc['1']['user_name'] == doc['1']['user_name']
    assert result_doc['2']['user_name'] == doc['2']['user_name']
    assert response.status == falcon.HTTP_OK

def test_post(client):
    """Tests post a user and ensures the response is 201"""
    params = {'name' : 'cody', 'email' : 'cody@codester.com', 'password' : 'pw'}
    kwargs = {'params' : params}
     
    response = client.simulate_post(path='/nerds', **kwargs)

    assert response.status == falcon.HTTP_201


