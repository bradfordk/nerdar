import json

from sqlalchemy import create_engine 
from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

from db.table_defs import *

import falcon

class GlobalGameRatingsResource(object):

    def on_get(self, req, resp):
        """Get a list of games and send to caller in JSON"""
        db_string = "postgresql://postgres:pw@localhost:5432/nerdar"
        engine = create_engine(db_string, echo=True)

        Session = sessionmaker(bind=engine)
        session = Session()

        ratings = {}

        for game in session.query(Games).join(GameShelf): 
            for rating in session.query(func.avg(GameShelf.rating).label('avg')).filter(GameShelf.game_id == game.game_id).filter(GameShelf.rating != None):
                ratings[game.game_id] = {"game_name" : game.game_name, "global_rating" : rating.avg}

        resp.body = json.dumps(ratings, ensure_ascii=False)

        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        pass

    def logging_func():
        pass

