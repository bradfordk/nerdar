import json

from sqlalchemy import create_engine 
from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from db.table_defs import *

import falcon

class GamesAtTimeResource(object):

    def on_get(self, req, resp, game_length):
        """Get a list of game names and ratings and send to caller in JSON"""
        db_string = "postgresql://postgres:pw@localhost:5432/nerdar"
        engine = create_engine(db_string, echo=True)

        Session = sessionmaker(bind=engine)
        session = Session()

        games = {}

        for game in session.query(Games).filter(Games.game_length == game_length):
            games[game.game_id] = {"game_name" : game.game_name, "game_type" : game.game_type, "game_length" : game.game_length}

        session.close() 

        resp.body = json.dumps(games, ensure_ascii=False)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        pass
