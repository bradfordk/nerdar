from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from table_defs import *

db_string = "postgresql://postgres:pw@localhost:5432/nerdar"
engine = create_engine(db_string, echo=True)

Session = sessionmaker(bind=engine)
session = Session()

user = Nerds("edgyusername", "user@usee.com", "parameterized")
session.add(user)

user = Nerds("neato", "neato@usee.com", "passworder")
session.add(user)

user = Nerds("zerg", "zerg@usee.com", "salted")
session.add(user)

user = Nerds("Taric", "Taric@usee.com", "encrypted")
session.add(user)

user = Nerds("Warlock", "warlock@usee.com", "8bit")
session.add(user)

game = Games("Dominion", "deck-builder", "30")
session.add(game)

game = Games("Viticulture", "worker-placement", "60")
session.add(game)

game = Games("Time Stories", "co-op", "300")
session.add(game)

game = Games("Twilight Struggle", "strategy", "120")
session.add(game)

game = Games("Mystic Vale", "deck-builder", "45")
session.add(game)

game_shelf = GameShelf("1", "1", "3", "15")
session.add(game_shelf)

game_shelf = GameShelf("1", "3", "7", "2")
session.add(game_shelf)

game_shelf = GameShelf("1", "5", "9", "40")
session.add(game_shelf)

game_shelf = GameShelf("2", "1", "9", "11")
session.add(user)

game_shelf = GameShelf("2", "2", "9", "21")
session.add(game_shelf)

game_shelf = GameShelf("2", "3", "5", "55")
session.add(game_shelf)

game_shelf = GameShelf("2", "4", "6", "22")
session.add(game_shelf)

session.commit()
session.close()

game_shelf = GameShelf("2", "5", "8", "94")
session.add(game_shelf)

game_shelf = GameShelf("3", "3", "4", "36")
session.add(game_shelf)

game_shelf = GameShelf("3", "4", "7", "7")
session.add(game_shelf)

game_shelf = GameShelf("4", "1", "9", "18")
session.add(game_shelf)

game_shelf = GameShelf("4", "2", "9", "77")
session.add(game_shelf)

game_shelf = GameShelf("4", "3", "5", "32")
session.add(game_shelf)

game_shelf = GameShelf("4", "4", "6", "28")
session.add(game_shelf)

game_shelf = GameShelf("4", "5", "8", "5")
session.add(game_shelf)

game_shelf = GameShelf("5", "2", "9", "52")
session.add(game_shelf)

game_shelf = GameShelf("5", "5", "2", "10")
session.add(game_shelf)

session.commit()
session.close()
