from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import UniqueConstraint
 
engine = create_engine('postgresql://postgres:pw@localhost:5432/nerdar', echo=True)
Base = declarative_base()
 
class Nerds(Base):
    """"""
    __tablename__ = "nerds"
    #sequence is a default generator
    nerd_id_seq = Sequence('nerd_id_seq', metadata=Base.metadata)
    nerd_id_seq.create(bind=engine)

    nerd_id = Column(
        Integer, nerd_id_seq,
        server_default=nerd_id_seq.next_value() ,primary_key=True)
    user_name = Column(String(20))
    email = Column(String(25), nullable=False, unique=True)
    password = Column(String(50))
    games = relationship("GameShelf", back_populates="nerds") #back populates nerds to games
    UniqueConstraint('email')

    def __init__(self, user_name, email, password):
        """"""
        self.user_name = user_name
        self.email = email
        self.password = password
        
class Games(Base):
    """"""
    __tablename__ = "games"
 
    game_id = Column(Integer, primary_key=True)
    game_name = Column(String(50), unique=True, nullable=False)
    game_type = Column(String(30), nullable=False) 
    game_length = Column(Integer, nullable=False)
    nerds = relationship("GameShelf", back_populates="games") #back populates games to nerds
    CheckConstraint('game_type in ("co-op", "deck-builder", "strategy", "worker-placement")')   

    def __init__(self, game_name, game_type, game_length):
        """"""
        self.game_name = game_name
        self.game_type = game_type
        self.game_length = game_length
        self.game_id

#association table containing extra columns than the FKs
class GameShelf(Base):
    """"""
    __tablename__ = "gameshelf"
 
    nerd_id = Column(Integer, ForeignKey("nerds.nerd_id"), primary_key=True)
    game_id = Column(Integer, ForeignKey("games.game_id"), primary_key=True)
    rating = Column(Float)
    plays = Column(Integer, default=0)
    CheckConstraint('0 < rating <= 10')
    games = relationship("Games", back_populates="nerds") #gives many to one (bidirectional relationship)
    nerds = relationship("Nerds", back_populates="games") #gives m -n 
    

    def __init__(self, nerd_id, game_id, rating, plays):
        """"""
        self.nerd_id = nerd_id
        self.game_id = game_id
        self.rating = rating
        self.plays = plays
 
# create tables
Base.metadata.create_all(engine)
