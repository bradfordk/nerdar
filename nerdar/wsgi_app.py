import falcon

from nerds.nerds import NerdsResource
from nerds.nerd import NerdResource
from games.games_at_time import GamesAtTimeResource
from games.global_game_ratings import GlobalGameRatingsResource

#Gunicorn expects application
api = application = falcon.API()

nerds = NerdsResource()
api.add_route('/nerds', nerds)

nerd = NerdResource()
api.add_route('/nerds/{email}', nerd)

games_at_time = GamesAtTimeResource()
api.add_route('/games/{game_length}', games_at_time)

global_game_ratings = GlobalGameRatingsResource()
api.add_route('/games/ratings', global_game_ratings)
