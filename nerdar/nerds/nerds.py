import json

from sqlalchemy import create_engine 
from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError

from db.table_defs import *

import falcon

class NerdsResource(object):

    def on_get(self, req, resp):
        """Get a list of users and send to caller in JSON"""
        db_string = 'postgresql://postgres:pw@localhost:5432/nerdar'
        engine = create_engine(db_string, echo=True)

        Session = sessionmaker(bind=engine)
        session = Session()

        nerds = {}

        for nerd in session.query(Nerds).order_by(Nerds.nerd_id):
            nerds[nerd.nerd_id] = {'user_name' : nerd.user_name, 'email' : nerd.email}

        resp.body = json.dumps(nerds, ensure_ascii=False)

        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        """Insert a user if it doesn't exist, otherwise return already exists message. Returns user URL either way"""
        name = req.get_param('name')
        email = req.get_param('email')
        pw = req.get_param('password')

        db_string = 'postgresql://postgres:pw@localhost:5432/nerdar'
        engine = create_engine(db_string, echo=True)

        Session = sessionmaker(bind=engine)
        session = Session()

        try:
            user = Nerds(name, email, pw)
            session.merge(user)

            session.commit()

        except IntegrityError:
            resp.body = json.dumps({1 : email + ' already exists'}, ensure_ascii=False)
        
        session.close()

        resp.status = falcon.HTTP_201
        resp.location = '/nerds/{' + email + '}'

    def setup_connection():
        pass
