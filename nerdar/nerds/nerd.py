import json

from sqlalchemy import create_engine 
from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from db.table_defs import *

import falcon

class NerdResource(object):

    def on_get(self, req, resp, email):
        """Get a list of users and send to caller in JSON"""
        db_string = "postgresql://postgres:pw@localhost:5432/nerdar"
        engine = create_engine(db_string, echo=True)

        Session = sessionmaker(bind=engine)
        session = Session()

        nerds = {}

        for nerd in session.query(Nerds).filter(Nerds.email == email):
            nerds[nerd.nerd_id] = {"user_name" : nerd.user_name, "email" : nerd.email}

        session.close()

        resp.body = json.dumps(nerds, ensure_ascii=False)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        """Add a user to the database and return status code and URL to root"""
        resp.status = falcon.HTTP_201
        resp.location = '/'

    def logging():
        pass
