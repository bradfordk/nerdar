INSTALL AND RUN (Instructions for Ubuntu 16.04. For Red Hat translate the apt-get instructions to yum, will update README for Red Hat in future release)

0. Install python3 (3.5 preferred) and pip3
   a. Install Python 3: sudo apt-get install python3
   b. Install pip3 : sudo apt-get install python3-pip
1. install postgresql : 
   a. sudo apt-get update
   b. sudo apt-get install postgresql postgresql-contrib
2. Install Gunicorn : pip3 install gunicorn
3. extract nerdar directory to /home/yourusername
     resulting dir looks like with ls -a : /home/yourusername/nerdar/[., .., .cache, .git, nerdar, .python3, README.md, tests]
4. open /home/{user}/nerdar/.python3/bin/activate
   Add export PYTHONPATH="/home/username/nerdar/nerdar" to end of file
5. open a separate terminal and sign into psql with postgres user
      1.  sudo -u postgres psql postgres
      2.  type: your local pw
      3.  type: \password postgres
                pw
                pw
6. in psql prompt create database nerdar
7. Use nerdar db : in psql prompt type \c nerdar
8. in first terminal ensure you are in /home/{user}/nerdar
10. set up virtual env type: source.python3/bin/activate
9. install db tables type: python3 nerdar/db/table_defs.py
10. fill tables type: python3 nerdar/db/dummy_fill_table.py
11. start application 
    a. cd nerdar/
    b. type: gunicorn wsgi_app:application
12. open browser to 127.0.0.1:8000/(whichever link you want to visit)
                 options listed in wsgi_app.py
                 example: 127.0.0.1:8000/games/45  > shows the list of games that last 45 minutes
                 exmaple: 127.0.0.1:8000/nerds > shows all the users
                 example: 127.0.0.1:8000/games/ratings > shows the aggregate rankings for each game
13. to run unit tests:
        1. navigate to ~/nerdar/
        2. type: pytest tests/test_games.py
        3. type: pytest tests/test_nerds.py



